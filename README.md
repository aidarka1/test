#Внутренниц Файлообменник#

##Выгрузка из ЕСА##
Команда - php bin/console userbundle:users:import

Формат xml
<RECORDS>
    <RECORD>
        <id>7</id>
        <username>Василий</username>
        <username_canonical>василий</username_canonical>
        <email>test2@test.ru</email>
        <email_canonical>test2@test.ru</email_canonical>
        <enabled>t</enabled>
        <salt>gtvo57bdf400o0gk88gwowoggwgk4so</salt>
        <password>109f4b3c50d7b0df729d299bc6f8e9ef9066971f</password>
        <locked>f</locked>
        <expired>f</expired>
        <roles>a:0:{}</roles>
        <credentials_expired>f</credentials_expired>
    </RECORD>
</RECORDS>


##RabbitMq##
Демон - php bin/console rabbitmq:consumer create_esa_user

