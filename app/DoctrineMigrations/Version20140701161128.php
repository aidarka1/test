<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140701161128 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("CREATE SEQUENCE outer_user_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
        $this->addSql("CREATE SEQUENCE outer_file_access_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
        $this->addSql("CREATE SEQUENCE outer_folder_access_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
        $this->addSql("CREATE TABLE outer_user (id INT NOT NULL, folder_root_id INT DEFAULT NULL, username VARCHAR(255) NOT NULL, username_canonical VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, email_canonical VARCHAR(255) NOT NULL, enabled BOOLEAN NOT NULL, salt VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, last_login TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, locked BOOLEAN NOT NULL, expired BOOLEAN NOT NULL, expires_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, confirmation_token VARCHAR(255) DEFAULT NULL, password_requested_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, roles TEXT NOT NULL, credentials_expired BOOLEAN NOT NULL, credentials_expire_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))");
        $this->addSql("CREATE UNIQUE INDEX UNIQ_D8B8B0BC92FC23A8 ON outer_user (username_canonical)");
        $this->addSql("CREATE UNIQUE INDEX UNIQ_D8B8B0BCA0D96FBF ON outer_user (email_canonical)");
        $this->addSql("CREATE UNIQUE INDEX UNIQ_D8B8B0BCC08504F5 ON outer_user (folder_root_id)");
        $this->addSql("COMMENT ON COLUMN outer_user.roles IS '(DC2Type:array)'");
        $this->addSql("CREATE TABLE outer_file_access (id INT NOT NULL, file_id INT DEFAULT NULL, user_id INT DEFAULT NULL, permission TEXT NOT NULL, PRIMARY KEY(id))");
        $this->addSql("CREATE INDEX IDX_1C57155793CB796C ON outer_file_access (file_id)");
        $this->addSql("CREATE INDEX IDX_1C571557A76ED395 ON outer_file_access (user_id)");
        $this->addSql("COMMENT ON COLUMN outer_file_access.permission IS '(DC2Type:array)'");
        $this->addSql("CREATE TABLE outer_folder_access (id INT NOT NULL, folder_id INT DEFAULT NULL, user_id INT DEFAULT NULL, permission TEXT NOT NULL, PRIMARY KEY(id))");
        $this->addSql("CREATE INDEX IDX_B8CD8AE9162CB942 ON outer_folder_access (folder_id)");
        $this->addSql("CREATE INDEX IDX_B8CD8AE9A76ED395 ON outer_folder_access (user_id)");
        $this->addSql("COMMENT ON COLUMN outer_folder_access.permission IS '(DC2Type:array)'");
        $this->addSql("ALTER TABLE outer_user ADD CONSTRAINT FK_D8B8B0BCC08504F5 FOREIGN KEY (folder_root_id) REFERENCES folder (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE outer_file_access ADD CONSTRAINT FK_1C57155793CB796C FOREIGN KEY (file_id) REFERENCES file (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE outer_file_access ADD CONSTRAINT FK_1C571557A76ED395 FOREIGN KEY (user_id) REFERENCES outer_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE outer_folder_access ADD CONSTRAINT FK_B8CD8AE9162CB942 FOREIGN KEY (folder_id) REFERENCES folder (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE outer_folder_access ADD CONSTRAINT FK_B8CD8AE9A76ED395 FOREIGN KEY (user_id) REFERENCES outer_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE outer_file_access DROP CONSTRAINT FK_1C571557A76ED395");
        $this->addSql("ALTER TABLE outer_folder_access DROP CONSTRAINT FK_B8CD8AE9A76ED395");
        $this->addSql("DROP SEQUENCE outer_user_id_seq CASCADE");
        $this->addSql("DROP SEQUENCE outer_file_access_id_seq CASCADE");
        $this->addSql("DROP SEQUENCE outer_folder_access_id_seq CASCADE");
        $this->addSql("DROP TABLE outer_user");
        $this->addSql("DROP TABLE outer_file_access");
        $this->addSql("DROP TABLE outer_folder_access");
    }
}
