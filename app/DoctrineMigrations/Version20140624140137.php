<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140624140137 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("CREATE SEQUENCE file_access_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
        $this->addSql("CREATE SEQUENCE folder_access_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
        $this->addSql("CREATE TABLE file_access (id INT NOT NULL, file_id INT DEFAULT NULL, user_id INT DEFAULT NULL, permission TEXT NOT NULL, PRIMARY KEY(id))");
        $this->addSql("CREATE INDEX IDX_BE7E0DEB93CB796C ON file_access (file_id)");
        $this->addSql("CREATE INDEX IDX_BE7E0DEBA76ED395 ON file_access (user_id)");
        $this->addSql("COMMENT ON COLUMN file_access.permission IS '(DC2Type:array)'");
        $this->addSql("CREATE TABLE folder_access (id INT NOT NULL, folder_id INT DEFAULT NULL, user_id INT DEFAULT NULL, permission TEXT NOT NULL, PRIMARY KEY(id))");
        $this->addSql("CREATE INDEX IDX_E3D15122162CB942 ON folder_access (folder_id)");
        $this->addSql("CREATE INDEX IDX_E3D15122A76ED395 ON folder_access (user_id)");
        $this->addSql("COMMENT ON COLUMN folder_access.permission IS '(DC2Type:array)'");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("DROP SEQUENCE file_access_id_seq CASCADE");
        $this->addSql("DROP SEQUENCE folder_access_id_seq CASCADE");
        $this->addSql("DROP TABLE file_access");
        $this->addSql("DROP TABLE folder_access");
    }
}
