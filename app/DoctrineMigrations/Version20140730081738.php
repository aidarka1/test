<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140730081738 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");

        $this->addSql("ALTER TABLE file ADD type VARCHAR(10) DEFAULT 0");
        $this->addSql("ALTER TABLE file ADD size INT DEFAULT NULL");
        $this->addSql("UPDATE file SET size = 1 WHERE size IS NULL");
        $this->addSql("ALTER TABLE file ALTER size SET NOT NULL");

    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE file DROP type");
        $this->addSql("ALTER TABLE file DROP size");
    }
}
