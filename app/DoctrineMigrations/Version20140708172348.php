<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140708172348 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE outer_user DROP CONSTRAINT fk_d8b8b0bcc08504f5");
        $this->addSql("DROP INDEX uniq_d8b8b0bcc08504f5");
        $this->addSql("ALTER TABLE outer_user DROP folder_root_id");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE outer_user ADD folder_root_id INT DEFAULT NULL");
        $this->addSql("ALTER TABLE outer_user ADD CONSTRAINT fk_d8b8b0bcc08504f5 FOREIGN KEY (folder_root_id) REFERENCES folder (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("CREATE UNIQUE INDEX uniq_d8b8b0bcc08504f5 ON outer_user (folder_root_id)");
    }
}
