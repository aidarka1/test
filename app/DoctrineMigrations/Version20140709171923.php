<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140709171923 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE fos_user ADD name VARCHAR(255) DEFAULT NULL");
        $this->addSql("ALTER TABLE fos_user ADD surname VARCHAR(255) DEFAULT NULL");
        $this->addSql("ALTER TABLE fos_user ADD patronymic VARCHAR(255) DEFAULT NULL");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE fos_user DROP name");
        $this->addSql("ALTER TABLE fos_user DROP surname");
        $this->addSql("ALTER TABLE fos_user DROP patronymic");
    }
}
