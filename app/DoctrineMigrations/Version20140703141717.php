<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140703141717 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("CREATE SEQUENCE event_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
        $this->addSql("CREATE TABLE user_event (user_id INT NOT NULL, event_id INT NOT NULL, PRIMARY KEY(user_id, event_id))");
        $this->addSql("CREATE INDEX IDX_D96CF1FFA76ED395 ON user_event (user_id)");
        $this->addSql("CREATE INDEX IDX_D96CF1FF71F7E88B ON user_event (event_id)");
        $this->addSql("CREATE TABLE event (id INT NOT NULL, user_id INT DEFAULT NULL, type VARCHAR(255) NOT NULL, name TEXT NOT NULL, full_text TEXT NOT NULL, create_date TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, emailText TEXT NOT NULL, PRIMARY KEY(id))");
        $this->addSql("CREATE INDEX IDX_3BAE0AA7A76ED395 ON event (user_id)");
        $this->addSql("ALTER TABLE user_event ADD CONSTRAINT FK_D96CF1FFA76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE user_event ADD CONSTRAINT FK_D96CF1FF71F7E88B FOREIGN KEY (event_id) REFERENCES event (id) ON DELETE CASCADE NOT DEFERRABLE INITIALLY IMMEDIATE");
        $this->addSql("ALTER TABLE event ADD CONSTRAINT FK_3BAE0AA7A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) NOT DEFERRABLE INITIALLY IMMEDIATE");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE user_event DROP CONSTRAINT FK_D96CF1FF71F7E88B");
        $this->addSql("DROP SEQUENCE event_id_seq CASCADE");
        $this->addSql("DROP TABLE user_event");
        $this->addSql("DROP TABLE event");
    }
}
