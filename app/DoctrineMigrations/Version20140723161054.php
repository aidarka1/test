<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140723161054 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE file ADD create_date TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL");
        $this->addSql("UPDATE file set create_date = now()");
        $this->addSql("ALTER TABLE file ALTER create_date DROP DEFAULT");
        $this->addSql("UPDATE folder set create_date = now()");
        $this->addSql("ALTER TABLE folder ALTER create_date DROP DEFAULT");

    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE file DROP create_date");
    }
}
