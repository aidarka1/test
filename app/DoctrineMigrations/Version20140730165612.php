<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140730165612 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("CREATE SEQUENCE logs_id_seq INCREMENT BY 1 MINVALUE 1 START 1");
        $this->addSql("CREATE TABLE logs (id INT NOT NULL, objectType INT NOT NULL, userType INT NOT NULL, userId INT NOT NULL, dateTime TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, PRIMARY KEY(id))");
        $this->addSql("ALTER TABLE file ALTER type DROP DEFAULT");
        $this->addSql("ALTER TABLE file ALTER type SET NOT NULL");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("DROP SEQUENCE logs_id_seq CASCADE");
        $this->addSql("DROP TABLE logs");
        $this->addSql("ALTER TABLE file ALTER type SET DEFAULT '0'");
        $this->addSql("ALTER TABLE file ALTER type DROP NOT NULL");
    }
}
