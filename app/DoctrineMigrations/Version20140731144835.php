<?php

namespace Application\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20140731144835 extends AbstractMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE logs ADD object_type INT NOT NULL");
        $this->addSql("ALTER TABLE logs ADD user_type INT NOT NULL");
        $this->addSql("ALTER TABLE logs ADD user_id INT NOT NULL");
        $this->addSql("ALTER TABLE logs DROP objecttype");
        $this->addSql("ALTER TABLE logs DROP usertype");
        $this->addSql("ALTER TABLE logs DROP userid");
        $this->addSql("ALTER TABLE logs RENAME COLUMN datetime TO date_time");
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() != "postgresql", "Migration can only be executed safely on 'postgresql'.");
        
        $this->addSql("ALTER TABLE logs ADD objecttype INT NOT NULL");
        $this->addSql("ALTER TABLE logs ADD usertype INT NOT NULL");
        $this->addSql("ALTER TABLE logs ADD userid INT NOT NULL");
        $this->addSql("ALTER TABLE logs DROP object_type");
        $this->addSql("ALTER TABLE logs DROP user_type");
        $this->addSql("ALTER TABLE logs DROP user_id");
        $this->addSql("ALTER TABLE logs RENAME COLUMN date_time TO datetime");
    }
}
