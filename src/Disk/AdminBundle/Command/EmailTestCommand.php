<?php

namespace Disk\AdminBundle\Command;

use FOS\UserBundle\Mailer\Mailer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class EmailTestCommand extends ContainerAwareCommand
{
    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('admin:email:test');
        }
    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $message = \Swift_Message::newInstance()
            ->setSubject('Файлобменник')
            ->setFrom('noreply.disk@tatneft.ru', 'Disk')
            ->setTo('regina.s-89@mail.ru')
            ->setBody('тестовое письмо');

        /** @var Mailer $mailer */
        $mailer = $this->getContainer()->get('mailer');
        $mailer->send($message);

    }
}
