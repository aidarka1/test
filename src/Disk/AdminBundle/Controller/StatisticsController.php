<?php
/**
 * User: Rashit Hamidullin Rashit.Hamidullin@gmail.com
 * Date: 31.07.14
 * Time: 15:58
 * 
 * 
 */

namespace Disk\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class StatisticsController extends Controller
{
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $innerUsersRepo = $em->getRepository('DiskUserBundle:User');

        $allInnerUsersCount = count($em->getRepository('DiskUserBundle:User')->findAll());
        $allOuterUsersCount = count($em->getRepository('DiskOuterAccessBundle:User')->findAll());
        $activeUsersCount = count($innerUsersRepo->getActiveUsers());

        $foldersCount = count($em->getRepository('DiskFilesBundle:Folder')->findAll());
        $filesCount = count($em->getRepository('DiskFilesBundle:File')->findAll());

        $innerFilesAccessRepo = $em->getRepository('DiskFilesBundle:FileAccess');
        $innderSharedFilesCount = count($innerFilesAccessRepo->getSharedFiles());

        $outerFilesAccessRepo = $em->getRepository('DiskOuterAccessBundle:FileAccess');
        $outeSharedFilesCount = count($outerFilesAccessRepo->getSharedFiles());


        return $this->render(
            'DiskAdminBundle:Statistics:index.html.twig',
            array(
                 'allInnerUsersCount' => $allInnerUsersCount,
                 'allOuterUsersCount' => $allOuterUsersCount,
                 'activeUsersCount'   => $activeUsersCount,
                 'filesCount'         => $filesCount,
                 'foldersCount'       => $foldersCount,
                 'sharedFilesCount'   => $innderSharedFilesCount,
                 'outeSharedFilesCount' => $outeSharedFilesCount,
            ));
    }
}