<?php

namespace Disk\AdminBundle\Controller;

use Disk\UserBundle\Entity\User;
use Disk\UserBundle\Form\UserType;
use Doctrine\ORM\EntityManager;
use Sit\FilesBundle\EventGenerator\ExtendUserEventGenerator;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

/**
 * User controller.
 *
 */
class UserController extends Controller
{

    /**
     * Lists all User entities.
     *
     */
    public function indexAction(Request $request)
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var Paginator $paginator */
        $paginator  = $this->get('knp_paginator');

        $defaultData = array('fio' => '');
        $form = $this->createFormBuilder($defaultData)
                    ->add('fio', 'text', array( 'required' => false, 'label' => 'ФИО' ))
                    ->add('submit', 'submit', array('label' => 'Искать'))
                    ->getForm();

        $form->handleRequest($request);
        $filter_data = $form->getData();

        $qb = $em->getRepository("DiskUserBundle:User")->createQueryBuilder('u');

        $users = $qb
            ->select('u')
            ->where('u <> :user')
            ->andWhere($qb->expr()->like('LOWER(CONCAT(u.surname, CONCAT(\' \', u.name), CONCAT(\' \',u.patronymic)))', ':keyword'))
            ->setParameters(array('user' => $this->getUser(), 'keyword' => '%' . mb_strtolower($filter_data['fio'], 'UTF-8') . '%'))
            ->getQuery()
        ;

        /** @var User[] $pagination */
        $pagination = $paginator->paginate(
            $users,
            $this->get('request')->query->get('page', 1),
            10
        );

        $pagination->setTemplate('DiskAdminBundle::twitter_bootstrap_pagination.html.twig');

        return $this->render('DiskAdminBundle:User:index.html.twig', array(
                'pagination' => $pagination,
                'form' => $form->createView()
            )
        );
    }

    /**
     * Finds and displays a User entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var User $entity */
        $entity = $em->getRepository('DiskUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }


        return $this->render('DiskAdminBundle:User:show.html.twig', array(
            'entity'      => $entity,
        ));
    }

    /**
     * Displays a form to edit an existing User entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var User $entity */
        $entity = $em->getRepository('DiskUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createEditForm($entity);

        return $this->render('DiskAdminBundle:User:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a User entity.
    *
    * @param User $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(User $entity)
    {
        $form = $this->createForm(new UserType(), $entity, array(
            'action' => $this->generateUrl('disk_admin_user_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->get('role_extended')->setData($entity->hasRole('ROLE_USER_EXTENDED'));
        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing User entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        /** @var User $entity */
        $entity = $em->getRepository('DiskUserBundle:User')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find User entity.');
        }

        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ($editForm->isValid()) {
            if ($editForm->get('role_extended')->getData() and
                !$entity->hasRole('ROLE_USER_EXTENDED')
            ) {
                $entity->addRole('ROLE_USER_EXTENDED');
                $this->get('sit_event.creator')->create(new ExtendUserEventGenerator($entity));
            } else {
                $entity->removeRole('ROLE_USER_EXTENDED');
            }
            $em->flush();

            return $this->redirect($this->generateUrl('disk_admin_user'));
        }

        return $this->render('DiskAdminBundle:User:edit.html.twig', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
        ));
    }
}
