<?php

namespace Disk\FilesBundle\Command;

use Disk\FilesBundle\Entity\File;
use Disk\FilesBundle\Entity\Folder;
use Doctrine\ORM\EntityManager;
use Sit\FilesBundle\Service\StructureManager;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class ClearOldStructureCommand extends ContainerAwareCommand
{
    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('sit:files:clear')
            ->setDescription('Clear old files and folders');
    }

    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $deletingFilesCount = $this->deleteFiles();
        $output->writeln("Count of deleting files: " . $deletingFilesCount . " \n");

        $deletingFoldersCount = $this->deleteFolders();
        $output->writeln("Count of deleting folders: " . $deletingFoldersCount . " \n");

    }

    private function deleteFiles()
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        /** @var StructureManager $structureManager */
        $structureManager = $this->getContainer()->get('sit_files.structure_manager');

        $connection = $em->getConnection();

        //Выбираем файлы, у которых есть хотя бы один расшаренный доступ и все доступы просрочены
        $statement = $connection->prepare("
            select f.id
            from file as f
            where
                  (
                    0 < (select count(*) from file_access sub_fa where f.id = sub_fa.file_id) OR
                    0 < (select count(*) from outer_file_access sub_ofa where f.id = sub_ofa.file_id)
                  ) and
                  0 = (select count(*) from file_access sub_fa2 where f.id = sub_fa2.file_id and sub_fa2.active_to > now() ) and
                  0 = (select count(*) from outer_file_access sub_ofa where f.id = sub_ofa.file_id and sub_ofa.active_to > now() )
        ");
        $statement->execute();
        $fileArray = $statement->fetchAll();


        $repo = $em->getRepository('DiskFilesBundle:File');
        foreach ($fileArray as $fileParams) {
            /** @var File $file */
            $file = $repo->find($fileParams['id']);

            //Удаление объекта в Swift
            $structureManager->getFileManager()->deleteFileFromFilesystem($file);
            $em->remove($file);
        }
        $em->flush();

        return count($fileArray);
    }

    private function deleteFolders()
    {
        /** @var EntityManager $em */
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $connection = $em->getConnection();

        //Выбираем папки, у которых есть хотя бы один расшаренный доступ и все доступы просрочены и это не рутовая папка и в ней нет файлов
        //Папки отсортированны по lvl, поэтому удаляются постепенно снизу вверх
        $statement = $connection->prepare("
            select f.id
            from folder as f
            where
              f.lvl != 0 and
              (
                0 < (select count(*) from folder_access sub_fa where f.id = sub_fa.folder_id)  OR
                0 < (select count(*) from outer_folder_access sub_ofa where f.id = sub_ofa.folder_id)
              ) and
              0 = (select count(*) from folder_access sub_fa2 where f.id = sub_fa2.folder_id and sub_fa2.active_to > now() ) and
              0 = (select count(*) from outer_folder_access sub_ofa where f.id = sub_ofa.folder_id and sub_ofa.active_to > now() ) and

              0 = (select count(*) from file ch_file where ch_file.folder_id = f.id )
            order by f.lvl DESC
        ");

        $statement->execute();
        $folderArray = $statement->fetchAll();

        $repoFolder = $em->getRepository('DiskFilesBundle:Folder');

        $deletedFoldersCount = 0;
        foreach ($folderArray as $folderParams) {
            /** @var Folder $folder */
            $folder = $repoFolder->find($folderParams['id']);

            //Проверяем, нет ли в папке других папок
            if (0 == count($folder->getChildren())) {
                $deletedFoldersCount++;
                $em->remove($folder);
                $em->flush();
            }
        }

        return $deletedFoldersCount;
    }
}