<?php
/**
 * User: Rashit Hamidullin Rashit.Hamidullin@gmail.com
 * Date: 31.07.14
 * Time: 16:26
 * 
 * 
 */

namespace Disk\FilesBundle\Repository;

use Doctrine\ORM\EntityRepository;

class FileAccessRepository extends EntityRepository
{
    public function getSharedFiles()
    {
        $qb = $this->createQueryBuilder('fa');

        $result = $qb
            ->select('f.id')
            ->leftJoin('DiskFilesBundle:File', 'f', 'WITH', 'fa.file = f.id')
            ->distinct(true)
            ->getQuery()
            ->getResult()
            ;

        return $result;
    }
}