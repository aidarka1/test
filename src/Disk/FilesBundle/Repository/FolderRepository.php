<?php

namespace Disk\FilesBundle\Repository;

use Sit\FilesBundle\Repository\FolderRepository as BaseRepository;

class FolderRepository extends BaseRepository
{
    public function getAllByRoot($root){
        $qb = $this->createQueryBuilder('f');
        $result = array();

        $folders = $qb
            ->select('f.id, f.name')
            ->where('f.root = :root')
            ->setParameter('root', $root)
            ->getQuery()
            ->getArrayResult()
        ;

        foreach($folders as $folder){
            $result[$folder['id']] = $folder['name'];
        }

        return $result;
    }

}