<?php

namespace Disk\FilesBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class DiskFilesBundle extends Bundle
{
    public function getParent()
    {
        return 'SitFilesBundle';
    }
}
