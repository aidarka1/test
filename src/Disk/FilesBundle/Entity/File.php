<?php

namespace Disk\FilesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sit\FilesBundle\Entity\AbstractFile as BaseFile;
use Sit\FilesBundle\Entity\AbstractFolder;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @ORM\Table(name="file")
 */
class File extends BaseFile
{

    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\ManyToOne(targetEntity="\Disk\FilesBundle\Entity\Folder", inversedBy="files")
     * @ORM\JoinColumn(name="folder_id", referencedColumnName="id")
     */
    protected $folder;
    /**
     * @ORM\OneToMany(targetEntity="\Disk\FilesBundle\Entity\FileAccess", mappedBy="file", cascade={"remove"})
     */
    protected $accesses;
    /**
     * @ORM\OneToMany(targetEntity="\Disk\OuterAccessBundle\Entity\FileAccess", mappedBy="file",cascade={"remove"})
     */
    protected $outerAccesses;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getOuterAccesses()
    {
        return $this->outerAccesses;
    }

    /**
     * @param mixed $outerAccesses
     */
    public function setOuterAccesses($outerAccesses)
    {
        $this->outerAccesses = $outerAccesses;
    }

    /**
     * Get folder
     *
     * @return \Disk\FilesBundle\Entity\Folder
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * Set folder
     *
     * @param AbstractFolder $folder
     * @return File
     */
    public function setFolder(AbstractFolder $folder = null)
    {
        $this->folder = $folder;

        return $this;
    }

    /**
     * Add accesses
     *
     * @param \Disk\FilesBundle\Entity\FileAccess $accesses
     * @return File
     */
    public function addAccess(\Disk\FilesBundle\Entity\FileAccess $accesses)
    {
        $this->accesses[] = $accesses;

        return $this;
    }

    /**
     * Remove accesses
     *
     * @param \Disk\FilesBundle\Entity\FileAccess $accesses
     */
    public function removeAccess(\Disk\FilesBundle\Entity\FileAccess $accesses)
    {
        $this->accesses->removeElement($accesses);
    }

    /**
     * Get accesses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAccesses()
    {
        return $this->accesses;
    }
}
