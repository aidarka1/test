<?php

namespace Disk\FilesBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Sit\FilesBundle\Entity\AbstractFile;
use Sit\FilesBundle\Entity\AbstractFolder as BaseFolder;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Table(name="folder")
 * @ORM\Entity(repositoryClass="\Disk\FilesBundle\Repository\FolderRepository")
 */
class Folder extends BaseFolder
{
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;
    /**
     * @ORM\OneToMany(targetEntity="\Disk\FilesBundle\Entity\File", mappedBy="folder", cascade={"remove"})
     */
    protected $files;
    /**
     * @ORM\OneToMany(targetEntity="\Disk\FilesBundle\Entity\FolderAccess", mappedBy="folder", cascade={"remove"})
     */
    protected $accesses;
    /**
     * @ORM\OneToMany(targetEntity="\Disk\OuterAccessBundle\Entity\FolderAccess", mappedBy="folder",cascade={"remove"})
     */
    protected $outerAccesses;
    /**
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="\Disk\FilesBundle\Entity\Folder", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $parent;
    /**
     * @ORM\OneToMany(targetEntity="\Disk\FilesBundle\Entity\Folder", mappedBy="parent", cascade={"remove"})
     * @ORM\OrderBy({"lft" = "ASC"})
     */
    protected $children;

    public function __construct()
    {
        parent::__construct();
        $this->files = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAccesses()
    {
        return $this->accesses;
    }

    /**
     * @param mixed $accesses
     */
    public function setAccesses($accesses)
    {
        $this->accesses = $accesses;
    }

    /**
     * @return mixed
     */
    public function getOuterAccesses()
    {
        return $this->outerAccesses;
    }

    /**
     * @param mixed $outerAccesses
     */
    public function setOuterAccesses($outerAccesses)
    {
        $this->outerAccesses = $outerAccesses;
    }

    /**
     * Get parent
     *
     * @return \Sit\FilesBundle\Entity\AbstractFolder
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set parent
     *
     * @param \Sit\FilesBundle\Entity\AbstractFolder $parent
     * @return \Sit\FilesBundle\Entity\AbstractFolder
     */
    public function setParent(\Sit\FilesBundle\Entity\AbstractFolder $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Add children
     *
     * @param \Sit\FilesBundle\Entity\AbstractFolder $children
     * @return \Sit\FilesBundle\Entity\AbstractFolder
     */
    public function addChild(\Sit\FilesBundle\Entity\AbstractFolder $children)
    {
        $this->children[] = $children;

        return $this;
    }

    /**
     * Remove children
     *
     * @param \Sit\FilesBundle\Entity\AbstractFolder $children
     */
    public function removeChild(\Sit\FilesBundle\Entity\AbstractFolder $children)
    {
        $this->children->removeElement($children);
    }

    /**
     * Get children
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add files
     *
     * @param \Sit\FilesBundle\Entity\AbstractFile $file
     * @return Folder
     */
    public function addFile(\Sit\FilesBundle\Entity\AbstractFile $file)
    {
        $this->files[] = $file;

        return $this;
    }

    /**
     * Remove files
     *
     * @param \Sit\FilesBundle\Entity\AbstractFile $file
     */
    public function removeFile(AbstractFile $file)
    {
        $this->files->removeElement($file);
    }

    /**
     * Get files
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFiles()
    {
        return $this->files;
    }
}
