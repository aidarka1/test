<?php

namespace Disk\FilesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sit\FilesBundle\Entity\AbstractFile;
use Sit\FilesBundle\Entity\FileAccess as BaseAccess;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="Disk\FilesBundle\Repository\FileAccessRepository")
 * @ORM\Table(name="file_access")
 */
class FileAccess extends BaseAccess
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\Disk\FilesBundle\Entity\File", inversedBy="accesses")
     * @ORM\JoinColumn(name="file_id", referencedColumnName="id")
     */
    private $file;

    /**
     * @ORM\ManyToOne(targetEntity="\Disk\UserBundle\Entity\User", inversedBy="fileAccesses")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get file
     *
     * @return \Disk\FilesBundle\Entity\File
     */
    public function getFile()
    {
        return $this->file;
    }

    public function setFile(AbstractFile $file = null)
    {
        $this->file = $file;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Disk\UserBundle\Entity\User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param UserInterface $user
     * @return $this|BaseAccess
     */
    public function setUser(UserInterface $user = null)
    {
        $this->user = $user;

        return $this;
    }
}
