<?php

namespace Disk\FilesBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Sit\EventBundle\Entity\Event as BaseEvent;
use Doctrine\ORM\Mapping\JoinTable;
/**
 * Событие
 *
 * @ORM\Table(name="event")
 * @ORM\Entity
 */
class Event extends BaseEvent
{

    /**
     * Создатель
     * @ORM\ManyToOne(targetEntity="\Disk\UserBundle\Entity\User", inversedBy="createdEvents")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $author;
    /**
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;
    /**
     * Получатели события
     * @ORM\ManyToMany(targetEntity="\Disk\UserBundle\Entity\User", inversedBy="events",cascade={"persist"})
     */
    private $users;
    /**
     * Внешние Получатели события
     * @ORM\ManyToMany(targetEntity="\Disk\OuterAccessBundle\Entity\User", inversedBy="events",cascade={"persist"})
     * @JoinTable(name="event_outer_user")
     */
    private $outerUsers;

    public function __construct()
    {
        parent::__construct();
        $this->users = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getOuterUsers()
    {
        return $this->outerUsers;
    }

    /*
     * Get id
     *
     * @return integer
     */

    /**
     * @param mixed $outerUsers
     */
    public function setOuterUsers($outerUsers)
    {
        $this->outerUsers = $outerUsers;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author)
    {
        $this->author = $author;
    }

    /**
     * Пользователе для оповещения.
     * @return mixed
     */
    function getUsers()
    {
        return $this->users;
    }

    /**
     * @param mixed $users
     * @return \Doctrine\Common\Collections\ArrayCollection
     */
    public function setUsers($users)
    {
        return $this->users = $users;
    }
}