<?php

namespace Disk\FilesBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sit\FilesBundle\Entity\AbstractFolder;
use Sit\FilesBundle\Entity\FolderAccess as BaseAccess;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="folder_access", uniqueConstraints={@ORM\UniqueConstraint(name="user_folder_idx", columns={"user_id", "folder_id"})})
 */
class FolderAccess extends BaseAccess
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\Disk\FilesBundle\Entity\Folder", inversedBy="accesses")
     * @ORM\JoinColumn(name="folder_id", referencedColumnName="id")
     */
    private $folder;

    /**
     * @ORM\ManyToOne(targetEntity="\Disk\UserBundle\Entity\User", inversedBy="folderAccesses")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     * @param AbstractFolder $folder
     * @return $this|BaseAccess
     */
    public function setFolder(AbstractFolder $folder = null)
    {
        $this->folder = $folder;

        return $this;
    }

    /**
     * Get file
     *
     * @return \Disk\FilesBundle\Entity\Folder
     */
    public function getFolder()
    {
        return $this->folder;
    }

    /**
     * @param UserInterface $user
     * @return $this|BaseAccess
     */
    public function setUser(UserInterface $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \Disk\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }
}
