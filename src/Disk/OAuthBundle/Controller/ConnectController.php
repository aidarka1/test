<?php

/*
 * This file is part of the HWIOAuthBundle package.
 *
 * (c) Hardware.Info <opensource@hardware.info>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Disk\OAuthBundle\Controller;

use HWI\Bundle\OAuthBundle\Security\Core\Authentication\Token\OAuthToken;
use HWI\Bundle\OAuthBundle\Security\Core\Exception\AccountNotLinkedException;
use HWI\Bundle\OAuthBundle\Controller\ConnectController as BaseController;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Disk\UserBundle\Entity\User;

/**
 * ConnectController
 *
 * @author Alexander <iam.asm89@gmail.com>
 */
class ConnectController extends BaseController
{
    /**
     * Shows a registration form if there is no user logged in and connecting
     * is enabled.
     *
     * @param Request $request A request.
     * @param string  $key     Key used for retrieving the right information for the registration form.
     *
     * @return Response
     *
     * @throws NotFoundHttpException if `connect` functionality was not enabled
     * @throws AccessDeniedException if any user is authenticated
     * @throws \Exception
     */
    public function registrationAction(Request $request, $key)
    {
        $connect = $this->container->getParameter('hwi_oauth.connect');
        if (!$connect) {
            throw new NotFoundHttpException();
        }

        $hasUser = $this->container->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED');
        if ($hasUser) {
            throw new AccessDeniedException('Cannot connect already registered account.');
        }

        $session = $request->getSession();
        $error = $session->get('_hwi_oauth.registration_error.'.$key);
        $session->remove('_hwi_oauth.registration_error.'.$key);

        if (!($error instanceof AccountNotLinkedException) || (time() - $key > 300)) {
            // todo: fix this
            throw new \Exception('Cannot register an account.');
        }

        $userInformation = $this
            ->getResourceOwnerByName($error->getResourceOwnerName())
            ->getUserInformation($error->getRawToken())
        ;

        // enable compatibility with FOSUserBundle 1.3.x and 2.x
        if (interface_exists('FOS\UserBundle\Form\Factory\FactoryInterface')) {
            $form = $this->container->get('hwi_oauth.registration.form.factory')->createForm();
        } else {
            $form = $this->container->get('hwi_oauth.registration.form');
        }

        $userData = $userInformation->getResponse();
        $request->setMethod('POST');


        /** @var \Disk\UserBundle\Repository\UserRepository $repo */
        $repo = $this->container->get('doctrine.orm.entity_manager')->getRepository('DiskUserBundle:User');
        $existUser = $repo->findOneBy(array('emailCanonical' => mb_strtolower($userData['email'])));
        $existUser2 = $repo->findOneBy(array('usernameCanonical' => mb_strtolower($userData['email'])));

        //Если пользователь существует, то выходим.
        if($existUser instanceof User){
            $user = $existUser;
        }elseif($existUser2 instanceof User){
            $user = $existUser2;
        }else{
            $user = new User();
            $user->setUsername($userData['email']);
            $user->setEmail($userData['email']);
            $user->setSurname($userData['surname']);
            $user->setName($userData['name']);
            $user->setPatronymic($userData['patronymic']);
            $password = substr(md5(uniqid(rand(), true)), 0, 6);
            $user->setPassword($password);
        }
        $form->setData($user);
        $this->container->get('hwi_oauth.account.connector')->connect($form->getData(), $userInformation);

        // Authenticate the user
        $this->authenticateUser($request, $form->getData(), $error->getResourceOwnerName(), $error->getRawToken());

        return new RedirectResponse($this->container->get('router')->generate('sit_files_my_files'));
    }

    /**
     * Connects a user to a given account if the user is logged in and connect is enabled.
     *
     * @param Request $request The active request.
     * @param string  $service Name of the resource owner to connect to.
     *
     * @throws \Exception
     *
     * @return Response
     *
     * @throws NotFoundHttpException if `connect` functionality was not enabled
     * @throws AccessDeniedException if no user is authenticated
     */
    public function connectServiceAction(Request $request, $service)
    {
        $connect = $this->container->getParameter('hwi_oauth.connect');
        if (!$connect) {
            throw new NotFoundHttpException();
        }

        //Проверяем есть ли связь у пользователя, которые уже авторизован.
        //Если да, то разлогинимся и отправляем на oauth registration
        $token = $this->container->get('security.context')->getToken();
        if($token){
            /** @var User $currentUser */
            $currentUser = $token->getUser();
            if($currentUser->getCoreId()){
                //Разлогиниться
                $this->container->get('security.context')->setToken(NULL);

                //редирект на hwi_oauth_connect
                return new RedirectResponse($this->container->get('router')->generate('hwi_oauth_connect'));

            }
        }


        $hasUser = $this->container->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED');
        if (!$hasUser) {
            throw new AccessDeniedException('Cannot connect an account.');
        }

        // Get the data from the resource owner
        $resourceOwner = $this->getResourceOwnerByName($service);

        $session = $request->getSession();
        $key = $request->query->get('key', time());

        if ($resourceOwner->handles($request)) {
            $accessToken = $resourceOwner->getAccessToken(
                $request,
                $this->container->get('hwi_oauth.security.oauth_utils')->getServiceAuthUrl($request, $resourceOwner)
            );

            // save in session
            $session->set('_hwi_oauth.connect_confirmation.'.$key, $accessToken);
        } else {
            $accessToken = $session->get('_hwi_oauth.connect_confirmation.'.$key);
        }

        $userInformation = $resourceOwner->getUserInformation($accessToken);

        // Show confirmation page?
        if (!$this->container->getParameter('hwi_oauth.connect.confirmation')) {
            goto show_confirmation_page;
        }

        // Handle the form
        /** @var $form FormInterface */
        $form = $this->container->get('form.factory')
            ->createBuilder('form')
            ->getForm();

        if ($request->isMethod('POST')) {
            $form->bind($request);

            if ($form->isValid()) {
                show_confirmation_page:

                /** @var $currentToken OAuthToken */
                $currentToken = $this->container->get('security.context')->getToken();
                $currentUser  = $currentToken->getUser();

                $this->container->get('hwi_oauth.account.connector')->connect($currentUser, $userInformation);

                if ($currentToken instanceof OAuthToken) {
                    // Update user token with new details
                    $this->authenticateUser($request, $currentUser, $service, $currentToken->getRawToken(), false);
                }


                return new RedirectResponse($this->container->get('router')->generate('sit_files_my_files', array(), UrlGeneratorInterface::ABSOLUTE_PATH));
            }
        }

        return $this->container->get('templating')->renderResponse('HWIOAuthBundle:Connect:connect_confirm.html.' . $this->getTemplatingEngine(), array(
            'key'             => $key,
            'service'         => $service,
            'form'            => $form->createView(),
            'userInformation' => $userInformation,
        ));
    }

    public function connectAction(Request $request)
    {
        $connect = $this->container->getParameter('hwi_oauth.connect');
        $hasUser = $this->container->get('security.context')->isGranted('IS_AUTHENTICATED_REMEMBERED');

        $error = $this->getErrorForRequest($request);

        // if connecting is enabled and there is no user, redirect to the registration form
        if ($connect
            && !$hasUser
            && $error instanceof AccountNotLinkedException
        ) {
            $key = time();
            $session = $request->getSession();
            $session->set('_hwi_oauth.registration_error.'.$key, $error);

            return new RedirectResponse($this->generate('hwi_oauth_connect_registration', array('key' => $key)));
        }

        if ($error) {
            // TODO: this is a potential security risk (see http://trac.symfony-project.org/ticket/9523)
            $error = $error->getMessage();
        }

        //TODO Нужно проверять наличие соединения с ЕСА. Если нет, то отобразить страницу с проблемой.
        $clientIp = $this->container->get('request')->getClientIp();

//        if($this->container->get('kernel')->getEnvironment() != 'prod' || substr($clientIp,0,3) == "10."){
            return new RedirectResponse($this->container->get('router')->generate('hwi_oauth_service_redirect', array('service' => 'core'), UrlGeneratorInterface::ABSOLUTE_PATH));
//        }

        return $this->container->get('templating')->renderResponse('HWIOAuthBundle:Connect:login.html.' . $this->getTemplatingEngine(), array(
            'error'   => $error,
        ));
    }

}