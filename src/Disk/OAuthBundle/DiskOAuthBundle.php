<?php

namespace Disk\OAuthBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class DiskOAuthBundle extends Bundle
{
    public function getParent()
    {
        return 'HWIOAuthBundle';
    }
}
