<?php

namespace Disk\UserBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class DiskUserBundle extends Bundle
{
    public function getParent()
    {
        return 'FOSUserBundle';
    }
}
