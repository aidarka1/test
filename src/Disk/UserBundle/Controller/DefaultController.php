<?php

namespace Disk\UserBundle\Controller;

use Doctrine\ORM\EntityManager;
use Sit\EventBundle\Entity\Event;
use Disk\UserBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        /** @var Paginator $paginator */
        $paginator  = $this->get('knp_paginator');

        /** @var User $user */
        $user = $this->getUser();

        $qb = $em->getRepository('DiskFilesBundle:Event')
            ->createQueryBuilder('e')
            ->select('e')
            ->innerJoin('e.users','u')
            ->where('u.id = :user')
            ->orderBy('e.createDate', 'desc')
            ->setParameter('user', $user)
            ->orderBy('e.createDate', 'DESC')
            ->getQuery();

        /** @var Event[] $events */
        $pagination = $paginator->paginate(
            $qb,
            $this->get('request')->query->get('page', 1),
            10
        );

        $user->setLastShowEventDate(new \DateTime());
        $em->persist($user);
        $em->flush();

        return $this->render('DiskUserBundle:Default:index.html.twig',array(
            'pagination' => $pagination
        ));
    }
}
