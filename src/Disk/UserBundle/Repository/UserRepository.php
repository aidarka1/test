<?php

namespace Disk\UserBundle\Repository;

use Sit\FilesBundle\Repository\UserRepository as BaseRepository;

class UserRepository extends BaseRepository
{
    public function getEmails()
    {
        $qb = $this->createQueryBuilder('u');

        $emails = array();

        $result = $qb->select('u.emailCanonical')
            ->getQuery()
            ->getResult()
        ;

        foreach ($result as $r) {
            $emails[] = $r['emailCanonical'];
        }

        return $emails;
    }
    public function getUserNames()
    {
        $qb = $this->createQueryBuilder('u');

        $userNames = array();

        $result = $qb->select('u.usernameCanonical')
            ->getQuery()
            ->getResult()
        ;

        foreach ($result as $r) {
            $userNames[] = $r['usernameCanonical'];
        }

        return $userNames;
    }


    public function getUserByFile($fileId)
    {
        $qb = $this->createQueryBuilder('u');

        $result = $qb
            ->innerJoin('u.folderRoot', 'rf')
            ->innerJoin('DiskFilesBundle:Folder', 'allf', 'WITH', 'allf.root = rf.id')
            ->innerJoin('allf.files', 'file')
            ->where('file.id = :fileId')
            ->setParameter('fileId', $fileId)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        return $result;
    }


    public function getUserByFolder($folderId)
    {
        $qb = $this->createQueryBuilder('u');

        $result = $qb
            ->innerJoin('u.folderRoot', 'rf')
            ->innerJoin('DiskFilesBundle:Folder', 'allf', 'WITH', 'allf.root = rf.id')
            ->where('allf.id = :folderId')
            ->setParameter('folderId', $folderId)
            ->getQuery()
            ->getOneOrNullResult()
        ;

        return $result;
    }

    public function getActiveUsers()
    {
        $qb = $this->createQueryBuilder('u');

        $result = $qb
            ->select('u')
            ->where('u.lastLogin IS NOT NULL')
            ->getQuery()
            ->getResult()
            ;
        return $result;
    }
}