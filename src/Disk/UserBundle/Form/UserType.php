<?php

namespace Disk\UserBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class UserType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', null, array('label' => 'Имя'))
            ->add('surname', null, array('label' => 'Фамилия'))
            ->add('patronymic', null, array('label' => 'Отчество'))
            ->add('locked', null, array('required' => false, 'label' => 'закрыть доступ'))
            ->add('role_extended', 'checkbox', array(
                'label' => 'Расширенная учетка',
                'mapped' => false,
                'required' => false,
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Disk\UserBundle\Entity\User'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'disk_userbundle_user';
    }
}
