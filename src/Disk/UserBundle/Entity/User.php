<?php

namespace Disk\UserBundle\Entity;

use Disk\FilesBundle\Entity\Folder;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Sit\EventBundle\Entity\Event;
use Sit\FilesBundle\Entity\AbstractFolder;
use Sit\FilesBundle\Entity\FileAccess;
use Sit\FilesBundle\Entity\FolderAccess;
use Sit\FilesBundle\Entity\User as BaseUser;

/**
 * @ORM\HasLifecycleCallbacks
 * @ORM\Entity(repositoryClass="Disk\UserBundle\Repository\UserRepository")
 * @ORM\Table(name="fos_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="Disk\FilesBundle\Entity\Event", mappedBy="author")
     */
    protected $createdEvents;

    /**
     * Получатели события
     * @ORM\ManyToMany(targetEntity="Disk\FilesBundle\Entity\Event", mappedBy="users")
     */
    protected $events;

    /**
     * @ORM\OneToMany(targetEntity="\Disk\FilesBundle\Entity\FolderAccess", mappedBy="user")
     */
    protected $folderAccesses;
    /**
     * @ORM\OneToMany(targetEntity="\Disk\FilesBundle\Entity\FileAccess", mappedBy="user")
     */
    protected $fileAccesses;
    /**
     * @ORM\OneToOne(targetEntity="\Disk\FilesBundle\Entity\Folder",cascade={"persist"})
     * @ORM\JoinColumn(name="folder_root_id", referencedColumnName="id")
     */
    protected $folderRoot;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $name;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $surname;
    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected $patronymic;
    /**
     * @var string
     *
     * @ORM\Column(name="core_id", type="string", nullable=true)
     */
    private $coreId;

    /**
     * @ORM\Column(name="last_show_event_date", type="datetime", nullable=true)
     */
    private $lastShowEventDate;

    public function __construct()
    {
        parent::__construct();
        $this->folderAccesses = new ArrayCollection();
        $this->fileAccesses = new ArrayCollection();
        $this->events = new ArrayCollection();
    }

    public function addEvent(Event $event)
    {
        $this->events->add($event);
    }

    /**
     * @return mixed
     */
    public function getEvents()
    {
        return $this->events;
    }

    /**
     * @param mixed $events
     */
    public function setEvents($events)
    {
        $this->events = $events;
    }

    /**
     * @return string
     */
    public function getCoreId()
    {
        return $this->coreId;
    }

    /**
     * @param string $coreId
     */
    public function setCoreId($coreId)
    {
        $this->coreId = $coreId;
    }

    /**
     * @return \Disk\FilesBundle\Entity\Folder
     */
    public function getFolderRoot()
    {
        return $this->folderRoot;
    }

    /**
     * @param AbstractFolder $folderRoot
     * @return $this|BaseUser
     */
    public function setFolderRoot(AbstractFolder $folderRoot = null)
    {
        $this->folderRoot = $folderRoot;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param FolderAccess $folderAccesses
     * @return $this|mixed
     */
    public function addFolderAccess(FolderAccess $folderAccesses)
    {
        $this->folderAccesses[] = $folderAccesses;

        return $this;
    }

    /**
     * @param FolderAccess $folderAccesses
     * @return mixed|void
     */
    public function removeFolderAccess(FolderAccess $folderAccesses)
    {
        $this->folderAccesses->removeElement($folderAccesses);
    }

    /**
     * Get folderAccesses
     *
     * @return \Doctrine\Common\Collections\Collection|\Disk\FilesBundle\Entity\FolderAccess $folders[]
     */
    public function getFolderAccesses()
    {
        //TODO параметризировать функцию: все или только активные(по умолчанию)
        //Todo отфильтровать from < now < to или если не сущеуствует from или to
        return $this->folderAccesses;
    }

    /**
     * @param FileAccess $fileAccesses
     * @return $this|BaseUser
     */
    public function addFileAccess(FileAccess $fileAccesses)
    {
        $this->fileAccesses[] = $fileAccesses;

        return $this;
    }

    /**
     * @param FileAccess $fileAccesses
     * @return mixed|void
     */
    public function removeFileAccess(FileAccess $fileAccesses)
    {
        $this->fileAccesses->removeElement($fileAccesses);
    }

    /**
     * Get fileAccesses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFileAccesses()
    {
        //TODO параметризировать функцию: все или только активные(по умолчанию)
        //Todo отфильтровать from < now < to или если не сущеуствует from или to
        return $this->fileAccesses;
    }

    /**
     * @ORM\PrePersist()
     */
    public function createRootFolder()
    {
        $rootFolder = new Folder();
        $rootFolder->setName('Документы ' . $this->getUsername());
        $this->setFolderRoot($rootFolder);
    }

    /**
     * Вернуть ФИО
     * @return string
     */
    public function getFio()
    {
        return $this->getSurname() . ' ' . $this->getName() . ' ' . $this->getPatronymic();
    }

    /**
     * @return mixed
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @param mixed $surname
     */
    public function setSurname($surname)
    {
        $this->surname = $surname;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getPatronymic()
    {
        return $this->patronymic;
    }

    /**
     * @param mixed $patronymic
     */
    public function setPatronymic($patronymic)
    {
        $this->patronymic = $patronymic;
    }

    /**
     * @param $lastShowEventDate
     * @return $this
     */
    public function setLastShowEventDate($lastShowEventDate)
    {
        $this->lastShowEventDate = $lastShowEventDate;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getLastShowEventDate()
    {
        return $this->lastShowEventDate;
    }
}
