<?php

namespace Disk\SynchronizationBundle\Rabbitmq\Callback;

use Disk\UserBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use OldSound\RabbitMqBundle\RabbitMq\ConsumerInterface;
use PhpAmqpLib\Message\AMQPMessage;

class CreateUserFromEsa implements ConsumerInterface
{
    /** @var  EntityManager */
    public $em;

    public function __construct($em)
    {
        $this->em = $em;
    }
    public function execute(AMQPMessage $msg)
    {
        $messageText = $msg->body;
        $userInfo = json_decode($messageText, true);
        $isUploadSuccess = $this->createUser($userInfo);
    }

    public function createUser($userInfo)
    {
        $surname = $userInfo['surname'];
        $name = $userInfo['name'];
        $patronymic = $userInfo['patronymic'];
        $email = $userInfo['email'];
        $userId = $userInfo['user_id'];
        $password = $userInfo['password'];

        //если не хватает параметров, то выходим
        if(!($email && $password)){
            return false;
        }

        /** @var \Disk\UserBundle\Repository\UserRepository $repo */
        $repo = $this->em->getRepository('DiskUserBundle:User');

        $existUser = $repo->findOneBy(array('emailCanonical' => mb_strtolower($email)));
        $existUser2 = $repo->findOneBy(array('usernameCanonical' => mb_strtolower($email)));

        //Если пользователь существует, то выходим.
        if($existUser instanceof User){
            $existUser->setCoreId($userId);
            $this->em->persist($existUser);
        }elseif($existUser2 instanceof User){
            $existUser2->setCoreId($userId);
            $this->em->persist($existUser2);
        }
        else{
            $user = new User();
            $user->setCoreId($userId);
            $user->setSurname($surname);
            $user->setName($name);
            $user->setPatronymic($patronymic);
            $user->setEmail($email);
            $user->setUsername($email);
            $user->setEnabled(true);
            $user->setPassword($password);
            $this->em->persist($user);
        }
        $this->em->flush();

        return true;

    }
}
