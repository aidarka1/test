<?php

namespace Disk\SynchronizationBundle\Command;

use Disk\UserBundle\Repository\UserRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Disk\UserBundle\Entity\User;

class CreateFromCoreUserCommand extends ContainerAwareCommand
{
    /**
     * @see Command
     */
    protected function configure()
    {
        $this
            ->setName('userbundle:users:import')
            ->setDescription('Load users from file')
            ->addArgument('filepath', InputArgument::OPTIONAL, 'Who do you want to greet?');
        }

    /**
     * @see Command
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $defaultFileName = "core_vvc_users.xml";
        $defaultFilePath = realpath($this->getContainer()->get('kernel')->getRootdir()."/../bin/");

        if ($input->isInteractive()){
            $dialog = $this->getHelperSet()->get('dialog');
            $userPath = $dialog->ask($output, "<question>Set path to file? (by default ".$defaultFilePath."/".$defaultFileName.")</question>\n");
        }

        if (is_null($userPath) === false) {
            $dumpFile = $userPath;
        }else{
            $dumpFile = $defaultFilePath."/"."/".$defaultFileName;
        }
        $total = 0;
        $totalError = 0;
        $unsavedUsers = array();
        $users = array();

        if (!file_exists($dumpFile)) {
            $output->writeln("<error>File not found or permission denied!</error>");
            return;
        }
        $objects = simplexml_load_file($dumpFile);

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        /** @var UserRepository $repo */
        $repo = $em->getRepository("DiskUserBundle:User");
        $emails = $repo->getEmails();
        $userNames = $repo->getUserNames();

        set_time_limit(0);

        foreach ($objects->RECORD as $key => $u)
        {
            if($key%1000 == 0){
                $em->flush();
            }

            if (
                in_array(strtolower($u->username), $userNames) === false &&
                in_array(strtolower($u->email_canonical), $emails) === false
            ){
                $user = new User();
                $user->setCoreId($u->id);
                $user->setEmail($u->username);
                $user->setUsername($u->username);
                $user->setName($u->name);
                $user->setSurname($u->surname);
                $user->setPatronymic($u->patronymic);
                $user->setPassword($u->password);
                $user->setEnabled(true);

                $em->persist($user);

                $total++;
            }
        }

        $em->flush();

        if ($totalError > 0) {
            $output->writeln("Total users count: ".$total." \n"."Errors count: ".$totalError." \n"."Unsaved users: \n".join(" \n", $unsavedUsers));
        }else{
            $output->writeln("Total users count: ".$total." \n");
        }
    }
}
