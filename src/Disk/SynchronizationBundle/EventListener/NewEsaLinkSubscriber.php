<?php
namespace Disk\SynchronizationBundle\EventListener;

use Disk\SynchronizationBundle\Service\NewLinkMessenger;
use Disk\UserBundle\Entity\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;


class NewEsaLinkSubscriber implements EventSubscriber
{
    /** @var  NewLinkMessenger */
    private $newLinkMessenger;

    public function __construct($newLinkMessenger){
        $this->newLinkMessenger = $newLinkMessenger;
    }

    public function getSubscribedEvents()
    {
        return array(
            'postPersist',
            'postUpdate',
        );
    }

    public function postUpdate(LifecycleEventArgs $args)
    {
        $changes = $args->getEntityManager()->getUnitOfWork()->getEntityChangeSet($args->getEntity());
        /** @var User $entity */
        $entity = $args->getObject();
        if(($entity instanceof User) && isset($changes['coreId'])){
            $this->index($args);
        }
    }

    public function postPersist(LifecycleEventArgs $args)
    {
        /** @var User $entity */
        $entity = $args->getObject();
        if(($entity instanceof User) && $entity->getCoreId()){
            $this->index($args);
        }
    }

    public function index(LifecycleEventArgs $args)
    {
        /** @var User $entity */
        $entity = $args->getObject();
        $this->newLinkMessenger->createMessage($entity);
    }
}