<?php

namespace Disk\SynchronizationBundle\Service;

use Disk\UserBundle\Entity\User;

class NewLinkMessenger
{
    /** @var \OldSound\RabbitMqBundle\RabbitMq\Producer  */
    private $producer;

    private $rabbitmqStatus;

    private $clientCode;

    public function __construct($producer, $rabbitmqStatus, $clientCode)
    {
        $this->producer = $producer;
        $this->producer->setContentType('application/json');
        $this->rabbitmqStatus = $rabbitmqStatus;
        $this->clientCode = $clientCode;
    }

    /**
     * @param User $user
     */
    public function createMessage(User $user)
    {
        $userInfo = array(
            'client_user_id' => $user->getId(),
            'esa_user_id' => $user->getCoreId(),
            'client_code' => $this->clientCode,
        );

        if($this->rabbitmqStatus === true){
            /**
             * Отправить в рабит
             */
            $this->sendToRabbitMq($userInfo);
        }

    }

    /**
     * Отправка в rabbitMq
     */
    private function sendToRabbitMq($userInfo)
    {
        $msg = $userInfo;
        $this->producer->publish(json_encode($msg));
    }

}