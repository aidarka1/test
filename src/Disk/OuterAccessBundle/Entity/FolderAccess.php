<?php

namespace Disk\OuterAccessBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Sit\FilesBundle\Entity\AbstractFolder;
use Sit\FilesBundle\Entity\FolderAccess as BaseAccess;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity
 * @ORM\Table(name="outer_folder_access", uniqueConstraints={@ORM\UniqueConstraint(name="outer_access_user_folder_idx", columns={"user_id", "folder_id"})})
 */
class FolderAccess extends BaseAccess
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="\Disk\FilesBundle\Entity\Folder", inversedBy="outerAccesses")
     * @ORM\JoinColumn(name="folder_id", referencedColumnName="id")
     */
    private $folder;

    /**
     * @ORM\ManyToOne(targetEntity="Disk\OuterAccessBundle\Entity\User", inversedBy="folderAccesses")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @param UserInterface $user
     * @return BaseAccess|void
     */
    public function setUser(UserInterface $user = null)
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set file
     * @param AbstractFolder $folder
     * @return $this|BaseAccess
     */
    public function setFolder(AbstractFolder $folder = null)
    {
        $this->folder = $folder;

        return $this;
    }

    /**
     * Get file
     *
     * @return \Disk\FilesBundle\Entity\Folder
     */
    public function getFolder()
    {
        return $this->folder;
    }
}
