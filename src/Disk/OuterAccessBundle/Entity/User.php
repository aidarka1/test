<?php

namespace Disk\OuterAccessBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Sit\EventBundle\Entity\Event;
use Sit\FilesBundle\Entity\AbstractFolder;
use Sit\FilesBundle\Entity\FileAccess as AbstractFileAccess;
use Sit\FilesBundle\Entity\FolderAccess as AbstractFolderAccess;
use Sit\FilesBundle\Entity\User as BaseUser;

/**
 * @ORM\Entity
 * @ORM\Table(name="outer_user")
 */
class User extends BaseUser
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\OneToMany(targetEntity="\Disk\OuterAccessBundle\Entity\FolderAccess", mappedBy="user",cascade={"remove"})
     */
    protected $folderAccesses;

    /**
     * @ORM\OneToMany(targetEntity="\Disk\OuterAccessBundle\Entity\FileAccess", mappedBy="user",cascade={"remove"})
     */
    protected $fileAccesses;

    /**
     * Получатели события
     * @ORM\ManyToMany(targetEntity="Disk\FilesBundle\Entity\Event", mappedBy="outerUsers")
     */
    protected $events;


    public function __construct()
    {
        parent::__construct();
        $this->folderAccesses = new ArrayCollection();
        $this->fileAccesses = new ArrayCollection();
    }


    /**
     * @return \Disk\FilesBundle\Entity\Folder
     */
    public function getFolderRoot()
    {
        return null;
    }

    /**
     * @param AbstractFolder $folderRoot
     * @return $this|BaseUser
     */
    public function setFolderRoot(AbstractFolder $folderRoot = null)
    {
        $this->folderRoot = $folderRoot;
        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \Sit\FilesBundle\Entity\FolderAccess $folderAccesses
     * @return $this|mixed
     */
    public function addFolderAccess(AbstractFolderAccess $folderAccesses)
    {
        $this->folderAccesses[] = $folderAccesses;

        return $this;
    }

    /**
     * @param FolderAccess $folderAccesses
     * @return mixed|void
     */
    public function removeFolderAccess(AbstractFolderAccess $folderAccesses)
    {
        $this->folderAccesses->removeElement($folderAccesses);
    }

    /**
     * Get folderAccesses
     *
     * @return \Doctrine\Common\Collections\Collection|\Disk\FilesBundle\Entity\FolderAccess $folders[]
     */
    public function getFolderAccesses()
    {
        //TODO параметризировать функцию: все или только активные(по умолчанию)
        //Todo отфильтровать from < now < to или если не сущеуствует from или to
        return $this->folderAccesses;
    }


    /**
     * @param FileAccess $fileAccesses
     * @return mixed|void
     */
    public function removeFileAccess(AbstractFileAccess $fileAccesses)
    {
        $this->fileAccesses->removeElement($fileAccesses);
    }

    /**
     * Get fileAccesses
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFileAccesses()
    {
        //TODO параметризировать функцию: все или только активные(по умолчанию)
        //Todo отфильтровать from < now < to или если не сущеуствует from или to
        return $this->fileAccesses;
    }

    /**
     * Add fileAccesses
     *
     * @param \Disk\OuterAccessBundle\Entity\FileAccess|\Sit\FilesBundle\Entity\FileAccess $fileAccesses
     * @return BaseUser
     */
    public function addFileAccess(AbstractFileAccess $fileAccesses)
    {

        $this->fileAccesses[] = $fileAccesses;

        return $this;
    }

    /**
     * Вернуть ФИО
     * @return string
     */
    public function getFio()
    {
        return $this->getEmail();
    }

    public function addEvent(Event $event){
    }
}
