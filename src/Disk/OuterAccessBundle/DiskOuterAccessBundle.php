<?php

namespace Disk\OuterAccessBundle;

use Symfony\Component\HttpKernel\Bundle\Bundle;

class DiskOuterAccessBundle extends Bundle
{
    public function getParent()
    {
        return 'SitOuterAccessBundle';
    }
}
