<?php

namespace Disk\MainBundle\Controller;

use Sit\EventBundle\Service\CreatorEvent;
use Sit\FilesBundle\EventGenerator\NewOuterUserEventGenerator;
use Sit\FilesBundle\Service\ActionManager;
use Sit\FilesBundle\Service\StructureManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class DefaultController extends Controller
{
    public function indexAction()
    {
        /** @var StructureManagerInterface $manager */
        $manager = $this->get('sit_files.structure_manager');

        /** @var ActionManager $actionManager */
        $actionManager = $this->get('sit_files.action_manager');

        $user = $this->getUser();

        $sharedFolders = $manager->getSharedFolders($user);
        $sharedFolderActions = array();
        foreach ($sharedFolders as $sharedFolder) {
            $sharedFolderActions[$sharedFolder->getId()] = $actionManager->getToolFolderActionsWithParams($sharedFolder, $user);
        }

        $sharedFiles = $manager->getSharedFiles($this->getUser());
        $sharedFileActions = array();
        foreach ($sharedFiles as $sharedFile) {
            $sharedFileActions[$sharedFile->getId()] = $actionManager->getToolFileActionsWithParams($sharedFile, $user);
        }

        return $this->render('DiskMainBundle:Default:index.html.twig', array(
            'sharedFiles' => $sharedFiles,
            'sharedFileActions' => $sharedFileActions,
            'sharedFolders' => $sharedFolders,
            'sharedFolderActions' => $sharedFolderActions,
        ));
    }
}
